'use strict';
const EventEmitter = require('events');
const http = require('http');
const zeromq = require('zeromq');
const socket = zeromq.socket('sub');
const winston = require('winston');

const logger = new winston.Logger({transports: [
  new winston.transports.Console({name:'info', level: 'info'}),
  new winston.transports.Console({name: 'error', level: 'error'})
]});

module.exports = class MetaAlertEnricherConnector extends EventEmitter {
  constructor () {
    super();
  }

  getCriticality(metaAlert){
    var options = {
      host: 'mair',
      port: 8080,
      method: 'POST',
      path: "/api/get-max-cvss-and-criticality-for-ip-list",
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }
    var self = this;
    var post_req = http.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        if(res.statusCode==200){
          //logger.log('info',"Response from CA OK(200), getting the max values from cvss and criticality.")
          var result=JSON.parse(chunk);
          var TargetIPs = metaAlert.Target[0].IP4;
          metaAlert.Target[0].MaxAssetCriticality=0;
          metaAlert.maxCvss=0;
          
          for (var i = 0, len = TargetIPs.length; i < len; i++) {
            if(result[TargetIPs[i]]!=undefined){
              if(result[TargetIPs[i]]['criticality']!=undefined && result[TargetIPs[i]]['criticality']>metaAlert.Target[0].MaxAssetCriticality){
                metaAlert.Target[0].MaxAssetCriticality=result[TargetIPs[i]]['criticality'];
              }
              if(result[TargetIPs[i]]['maxCvss']!=undefined && result[TargetIPs[i]]['maxCvss']>metaAlert.maxCvss){
                metaAlert.maxCvss=result[TargetIPs[i]]['maxCvss'];
              }
            }
          }
          var  SourceIPs = metaAlert.Source[0].IP4;
          metaAlert.Source[0].MaxAssetCriticality=0;
          for (var i = 0, len = SourceIPs.length; i < len; i++) {
            if(result[SourceIPs[i]]!=undefined){
              if(result[SourceIPs[i]]['criticality']!=undefined && result[SourceIPs[i]]['criticality']>metaAlert.Source[0].MaxAssetCriticality){
                metaAlert.Source[0].MaxAssetCriticality=result[SourceIPs[i]]['criticality'];
              }
              if(result[SourceIPs[i]]['maxCvss']!=undefined && result[SourceIPs[i]]['maxCvss']>metaAlert.maxCvss){
                metaAlert.maxCvss=result[SourceIPs[i]]['maxCvss'];
              }
            }
          }
          metaAlert.TopAssetCriticality=Math.max(metaAlert.Source[0].MaxAssetCriticality,metaAlert.Target[0].MaxAssetCriticality);
          self.emit('meta-alert-with-criticality', metaAlert);
        }else{
          logger.log('error',"Response from CA ("+res.statusCode+") Source ips:"+JSON.stringify(metaAlert.SourceIPs)+" Target ips: "+JSON.stringify(metaAlert.TargetIPs));
        }
      });
      res.on('error', (e) => {
        logger.log(`Problem sending to CA: ${e.message}`);
      });
    });


    var ips = metaAlert.Source[0].IP4.concat(metaAlert.Target[0].IP4);
    
    if(ips.length>0){
      try{
        post_req.write(JSON.stringify(ips));
        post_req.end();
      }catch(err){
        logger.log('error', "[CA CONNECTOR]: Storing meta-alert without criticlaity error message: "+err.message);
        this.emit('meta-alert-with-criticality', metaAlert);
        post_req.abort();
      }
      
    }else{
      post_req.abort();
    }

 
  }
  destroy () {
    socket.close();
  }
}
