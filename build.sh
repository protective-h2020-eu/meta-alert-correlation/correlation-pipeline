#!/bin/sh
sudo docker stop node-gmv
sudo docker rm node-gmv

rm logs/*

sudo docker build -t node-gmv .
sudo docker tag node-gmv registry.gitlab.com/protective-h2020-eu/meta-alert-correlation/correlation-pipeline
sudo docker push registry.gitlab.com/protective-h2020-eu/meta-alert-correlation/correlation-pipeline
#sudo docker run --name=node-gmv -d node-gmv #--network="host" -d node-gmv 
#sudo docker logs node-gmv -f
