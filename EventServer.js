'use strict';
const EventEmitter = require('events');
const net = require('net');

//server publisher code:
module.exports = class EventServer extends EventEmitter {
  constructor (port) {
    super();
    this.clients = new Array();
    this.server = net.createServer((conn) => {
      conn.on('end', () => {
        this.emit('clientDisconnect');
        this.clients.splice(this.clients.indexOf(conn), 1);
        conn.destroy();
      });
      conn.on('error', (err) => {
        this.emit('clientDisconnect', {
          error: err
        });
        this.clients.splice(this.clients.indexOf(conn), 1);
      });
      clients.push(conn);
      this.emit('clientConnect', {
        client: conn,
        qClient: this.clients.length
      });
    });
    this.server.on('error', (err) => {
      this.emit('error', {
        error: err
      });
    });
    this.server.listen(port, () => {
      this.emit('listening');
    });
  }
  getClients () {
    return JSON.parse(JSON.stringify(this.clients));
  }
  sendEventToClients (event) {
    for (let i = 0; i < this.clients.length; i++) {
      clients[i].write(JSON.stringify(event)+"\n");
    }
  }
  destroy () {
    for (let i = 0; i < this.clients.length; i++) {
      this.clients[i].destroy();
    }
    this.server.close();
  }
}
