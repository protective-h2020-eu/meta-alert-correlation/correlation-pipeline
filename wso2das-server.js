'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const ideaFormatter = require('./IDEAFormatter.js');
const EventEmitter = require('events');

const	port = 4004;


module.exports = class Wso2dasServer extends EventEmitter {
  constructor () {
    super();
    const	app = express();
    const server = require('http').createServer(app);
    app.use(bodyParser.json({limit: '50mb'}));       // to support JSON-encoded bodies

    app.route('/meta-alert').post( (req, res) => {
      let alert = req.body;
      alert = ideaFormatter.buildMetaAlert(alert);
      this.emit('meta-alert', alert);
      res.status(201).end();
    });

    server.listen(port);
  }

  destroy () {
    server.close();
  }
}
