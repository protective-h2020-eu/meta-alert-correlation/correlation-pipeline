'use strict';

const winston = require('winston');
const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      name: 'info',
      //filename: __dirname+'/logs/info.mentat2node.log',
      level: 'info'
    }),
    new winston.transports.Console({
      name: 'error',
      //filename: __dirname+'/logs/error.mentat2node.log',
      level: 'error'
    })
  ]
});

//ZMQ client. Subscribes to the IDEA events published at the Mentat pipeline
const EventEmitter = require('events'),
      zeromq = require('zeromq'),
      socket = zeromq.socket('sub');

module.exports = class MentatClient extends EventEmitter {
  constructor (mentatURL) {
    super();
    socket.on('message', (topic, request) => {
      logger.log('info','[MentatClient]: Alert received.')
      const event = request.toString();
      this.emit('ideaEvent', {
        event
      });
    });
    socket.connect(mentatURL);
    socket.subscribe('idea_event');
    this.emit('subscribe');
  }

  destroy () {
    socket.close();
  }
}
