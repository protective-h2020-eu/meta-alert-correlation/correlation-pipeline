'use strict';

const fs = require('fs');
const winston = require('winston');
const ideaFormatter = require('./IDEAFormatter.js');
const IdeaEmitter = require('./EventHttpEmitter.js');
const MentatClient = require('./MentatClient.js');
const MetaAlertEnricherConnector = require('./MetaAlertEnricherConnector.js');
const Wso2dasServer = require ('./wso2das-server.js');

const ideaEmitter = new IdeaEmitter('protective_wso2-das_1', 9763);
const alertClient = new MentatClient('tcp://protective_mentat_1:6969')
const metaAlertClient = new MetaAlertEnricherConnector();
const wso2DasReceiver = new Wso2dasServer();
const logger = new winston.Logger({transports: [
  new winston.transports.Console({name:'info', level: 'info'}),
  new winston.transports.Console({name: 'error', level: 'error'})
]});

// 1. ALERT PROCESSING AND SENDING TO WSO2
alertClient.on('subscribe', () => {
  logger.log('info',"[Alert Client] Subscriber connected to mentat Alert queue...");
});

alertClient.on('ideaEvent', (payload) => {
  logger.log('info',"[Alert Client] The received alert is: "+payload.event);
  try {
    sendToDAS(payload.event);
    logger.log('info',"[Alert Client] Alert sent to WSO2 successfully");

  }catch (error) {
    logger.log('error',"[Emitter to DAS] Error: "+error);
  }
});
alertClient.on('error', (e) => {
  logger.log(`Problem receiving alerts from MENTAT: ${e.message}`);
});

ideaEmitter.on('error', (e) => {
  logger.log(`Problem sending to WSO2: ${e.message}`);
})
metaAlertClient.on('error', (e) => {
  logger.log(`Problem with CA: ${e.message}`);
})
   

// 2. PREPARING THE EVENT TO WSO2 AND SENDING IT
function sendToDAS (event) {
  event = JSON.parse(event);
  event = ideaFormatter.buildIdeaEvent(event);
  const targets = event.Target;
  const sources = event.Source;
  const categories = event.Category;
  const AlertQuality = event.AlertQuality;

  for (let i = 0; i < targets.length; i ++) {
    for (let j = 0; j < Math.max(targets[i].IP4.length,1); j++) {
      let target = ideaFormatter.extractDasObject(targets[i], event, j);
      ideaEmitter.sendIdeaEvent(JSON.stringify(target), '/endpoints/TargetHTTP');
      //logger.log('info', "[SENDING TARGET]: "+ JSON.stringify(target));
    }
  }
  for (let i = 0; i < sources.length; i ++) {
    for (let j = 0; j < Math.max(sources[i].IP4.length,1); j++) {
      let source = ideaFormatter.extractDasObject(sources[i], event,j);
      ideaEmitter.sendIdeaEvent(JSON.stringify(source), '/endpoints/SourceHTTP');
      //logger.log('info', "[SENDING SOURCE]: "+ JSON.stringify(source));
    }
  }
  for (let i = 0; i < categories.length; i++) {
    let category = ideaFormatter.extractDasCategory(categories[i], event);
    ideaEmitter.sendIdeaEvent(JSON.stringify(category), '/endpoints/CategoryHTTP');
    //logger.log('info', "[SENDING CATEGORY]: "+ JSON.stringify(category));
  }
  let quality = ideaFormatter.extractDasQuality(AlertQuality, event);
  ideaEmitter.sendIdeaEvent(JSON.stringify(quality), '/endpoints/QualityHTTP');
  //logger.log('info', "[SENDING AlertQuality]: "+ JSON.stringify(quality));

}
// 3. RECEIVED META-ALERT FROM WSO2, META-ALERTS SHARING & ENRICHMENT
wso2DasReceiver.on('meta-alert', (metaAlert) => {
  logger.log('info','Received from wso2');
  // Categroy field to array and add MetaAlert Categroy.
  metaAlert.Category = split(metaAlert.Category);
  metaAlert.Category.push('MetaAlert');

 // Make the Source and Target  arrays
  let sourceArr = [];
  let targetArr = [];

  sourceArr.push(metaAlert.Source);
  targetArr.push(metaAlert.Target);

  metaAlert.Source = sourceArr;
  metaAlert.Target = targetArr;

  metaAlert.AggrID = metaAlert.AggrID.split(',');

  for (let i = 0; i < metaAlert.Source.length; i ++) {
    metaAlert.Source[i].Hostname = split(metaAlert.Source[i].Hostname);
    metaAlert.Source[i].IP4 = split(metaAlert.Source[i].IP4);
    metaAlert.Source[i].Proto = split(metaAlert.Source[i].Proto);
    metaAlert.Source[i].Port = split(metaAlert.Source[i].Port.replace(/\[/g, "").replace(/\]/g,""));
    metaAlert.Source[i].MaxAssetCriticality = 0;
  }

  for (let i = 0; i < metaAlert.Target.length; i ++) {
    metaAlert.Target[i].Hostname = split(metaAlert.Target[i].Hostname);
    metaAlert.Target[i].IP4 = split(metaAlert.Target[i].IP4);
    metaAlert.Target[i].Proto = split(metaAlert.Target[i].Proto);
    metaAlert.Target[i].Port = split(metaAlert.Target[i].Port.replace(/\[/g,"").replace(/\]/g,""))
    metaAlert.Target[i].MaxAssetCriticality = 0;
  }
  metaAlert.TopAssetCriticality = 0;

  // Store the Meta-alerts in folder to be shared without CA information

  fs.writeFile('/var/meta-alerts/'+metaAlert.ID.split(',')[0]+'.metaalert',JSON.stringify(metaAlert),'utf8',function(){
    logger.log('info','[PipelineHandler]: Saving Meta-alert to be shared:'+metaAlert.ID.split(',')[0]+'.metaalert')
  });

  // Enrich the Meta-alerts with CA information
  logger.log('info', "[CA CONNECTOR]: Getting meta-alert criticality and storing");
  metaAlertClient.getCriticality(metaAlert);

});

wso2DasReceiver.on('error', (e) => {
  logger.log(`Problem receiving from WSO2: ${e.message}`);
});

// 4. STORAGE OF THE META-ALERT AFTER THE SHARING AND WITH CA INFO
metaAlertClient.on('meta-alert-with-criticality', (metaAlert) => {
  
  // Store the internal Meta-alerts through the Mentat pipeline
  fs.writeFile('/var/internal-meta-alerts/'+metaAlert.ID.split(',')[0]+'.metaalert',JSON.stringify(metaAlert),'utf8',function(){
    logger.log('info','[PipelineHandler]: Saving internal Meta-alert:'+metaAlert.ID.split(',')[0]+'.metaalert')
  });

});

ideaEmitter.on('send-error', (err) => {
  logger.log('error',"[Emitter to DAS] Error: "+error);
});

process.on('SIGINT', () => {
  alertClient.destroy();
  ideaEmitter.destroy();
  wso2DasReceiver.destroy();
  metaAlertClient.destroy();
});

function split(input){
  var result = input.split(",")
  var resultNoEmptyValues = [];
  for (let i = 0; i < result.length; i ++) {
      if (result[i] != "" ){
          resultNoEmptyValues.push(result[i]);
      }      
  }
  return resultNoEmptyValues;
}
