'use strict';

const net = require('net');
const winston = require('winston');

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      name: 'info',
      //filename: __dirname+'/logs/info.mentat2node.log',
      level: 'info'
    }),
    new winston.transports.Console({
      name: 'error',
      //filename: __dirname+'/logs/error.mentat2node.log',
      level: 'error'
    })
  ]
});

const clients = new Array();
const server = net.createServer((conn) => {
  conn.on('end', () => {
    logger.log('info', 'client disconnected', {
      message: 'client disconnected',
      type: 'socket connection'
    });
    clients.splice(clients.indexOf(conn), 1);
    conn.destroy();
  });
  conn.on('error', (err) => {
    logger.log('error', err, {
      message: err,
      type: 'socket error'
    });
    clients.splice(clients.indexOf(conn), 1);
  });
  conn.on('data', (data) => {
    logger.log('info', 'message:'+data.toString());
  });
  clients.push(conn);
  logger.log('info',"client connected. total clients:"+clients.length);
});

server.listen(3004, () => {
  logger.log('info', "server bound to port 3004");
});
server.on('error', (err) => {
  logger.log('error', err, {
    message: error,
    type: 'critical server error'
  });
});
