#!/bin/bash
echo "Waiting WSO2 Service to start..."
while [[ "$(curl -I -X GET -s -k https://wso2-das:9443/carbon/admin/login.jsp | head -1 | awk -F ' ' '{print $2}')" != "200" ]]; do
   sleep 1
done
echo "WSO2 ready, let's start the correlation pipeline"
exec npm start
