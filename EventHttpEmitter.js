'use strict';
//const EventEmitter = require('events');
const http = require('http'),  pool = new http.Agent( {maxSockets: 1} );
const EventEmitter = require('events');
//server publisher code:
module.exports = class EventHttpEmitter extends EventEmitter { //extends EventEmitter {
  constructor (host, port) {
    super();
    this.host = host;
    this.port = port;
  }
  sendIdeaEvent (event, endpoint = '/') {
    try {
      let options = {
        port: this.port,
        host: this.host,
        method: 'POST',
        path: endpoint,
        agent:pool,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic YWRtaW46YWRtaW4=',
          'Content-Length': Buffer.byteLength(event)
        }
      };
      let request = http.request(options);
      request.on('error', (e) => {
        console.log(e);
      });
      request.end(event);
    }catch (err) {
      this.emit('send-error', err);
    }
  }
  destroy () {

  }
}
