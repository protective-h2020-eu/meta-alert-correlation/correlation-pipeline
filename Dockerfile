FROM node:carbon

#create app dir
WORKDIR /usr/src/app

COPY package*.json ./
COPY wait-wso2_service.sh ./

RUN npm install

COPY . .

EXPOSE 4004

ENTRYPOINT ["./wait-wso2_service.sh"]
