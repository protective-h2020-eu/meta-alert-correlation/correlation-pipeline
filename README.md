# GMV Correlation Pipeline

This software creates a pipeline which starts receiving the events emitted by the **Mentat** module ***mentat-enriched-extractor***.

The next step of the pipeline is send the events in an appropriate format to the correlation engine **Wso2DAS**.

The pipeline listens in parallel the *meta-alert* sent by **Wso2DAS** and forwards it to **Mentat** again.


Diagram of the above explained:
[diagram]

[diagram]: https://gitlab.com/GMV-Protective-Correlation/gmv-correlation-pipeline/blob/master/mentat-enriched-extractor.jpg

Due to the characteristics of this pipeline, we are using [Node.js](https://nodejs.org/en/docs/) as it's quite good for [scheduling purposes](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/).

## PipelineHandler.js
This file is contains the code that *orchestrates* the **GMV Correlation Pipeline**.

It uses the classes needed for each step in the pipeline.

### MentatClient.js
This class connects to **Mentat** using the paradigm pub/sub and the library [ZMQ](http://zeromq.org/) for that purpose. It emits events so that the class that uses *MentatClient.js* is notified whenever an event from **Mentat** is captured.

### IDEAFormatter.js
This class has the necessary methods to parse:
* The events received from **Mentat**.
* Parse events in an appropriate format to send them to **Wso2DAS**.
* Parse the incoming ***meta-alerts***.

The methods within this class can be tested running `npm run test` from the root of the project.

### EventHttpEmitter.js
This class is used to send the events collected from **Mentat** to **Wso2DAS**.

### wso2das-server.js
This class is used to get the ***meta-alerts*** from **Wso2DAS** back to the pipeline.
