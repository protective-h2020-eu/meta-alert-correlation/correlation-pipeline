const expect = require('chai').expect;
const formatter = require('../IDEAFormatter.js');

describe("tests for incoming events from Mentat", () => {
  var testEvent = {
     "Format": "IDEA0",
     "ID": "testmsg-czqzdsne4x6t2ua5wf6b",
     "DetectTime": "2017-12-19T12:46:27Z",
     "Category": ["Attempt.Exploit","Test"],
     "ConnCount": 633,
     "Description": "Ping scan",
     "Source": [
        {
           "IP4": ["10.0.0.4"],
           "Proto": ["icmp"]
        }
     ],
     "Target": [
        {
           "Proto": ["icmp"],
           "IP4": ["195.113.144.230"],
           "Anonymised": true
        }
     ],
     "Node" : [
        {
           "SW" : ["warden_filer"],
           "Name" : "cz.cesnet.mentat"
        },
        {
           "SW" : ["FTAS"],
           "Name" : "cz.cesnet.lister"
        }
     ]
  };
  it ("should not throw an exception", () => {
    var rawFunction = () => {
      formatter.buildIdeaEvent();
    };
    var paramFunction = () => {
      formatter.buildIdeaEvent(testEvent);
    };
    expect(rawFunction).to.throw('Event argument expected');
    expect(paramFunction).not.to.throw();
  });
  it ("event should not be undefined nor null", () => {
    var event = formatter.buildIdeaEvent(testEvent);
    expect(event).not.to.be.null;
  });
  after(() => {
    console.log("---------------------------");
    console.log(JSON.stringify(formatter.buildIdeaEvent(testEvent)));
    console.log("---------------------------");
  });
});

describe ('Parsing of Events arrived from wso2 DAS', () => {
  const metaEvent = {
    "event":{
      "metaData":{
        "ID":"id_1",
        "DetectTime":"dia_1"
      },
      "payloadData":{
        "Category":"Attempt.Login",
        "AggrID":" ",
        "TopAssetCriticality":1,
        "Rule":2,
        "Source_Asset":" ",
        "Source_MaxAssetCriticality":1,
        "Source_IP4":"Source_2",
        "Source_Proto":"TCP",
        "Source_Hostname":"Source_2",
        "Target_Asset":" ",
        "Target_MaxAssetCriticality":1,
        "Target_IP4":"Target_1",
        "Target_Proto":"TCP",
        "Target_Hostname":"Target_1"
      }
    }
  };
  it ('Should contain all the keys', () => {
    const wso2MetaTemplate = require('../wso2das-meta-alert.json');
    let badMeta = {
      "event":{
        "metaData":{
          "ID":"id_1",
          "EventTime":"dia_1"
        },
        "payloadData":{
          "Category":"Attempt.Login",
          "AggrID":" ",
          "TopAssetCriticality":1,
          "Rule":2,
          "Source_Asset":" "
        }
      }
    };
    expect(badMeta).to.not.have.property(wso2MetaTemplate);
  });
  it ('should return a well formed IDEA meta event', () => {
    const ideaMetaTemplate = require('../IDEAMetaAlert.json');
    let metaOut = formatter.buildMetaAlert(metaEvent);
    expect(metaOut).to.have.deep.keys(ideaMetaTemplate);
    console.log(metaOut);
  });
});
