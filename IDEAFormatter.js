'use strict';

const ideaEvent = require('./IDEAEvent.json');
const metaAlert = require('./IDEAMetaAlert.json');

const COMPLEX_KEYS = ["Source", "Target", "Node", "Attach"];

//PRE: event is a JSON object (stringified)
function buildIdeaEvent (event) {
  if (!event) throw Error('Event argument expected');

  event = JSON.parse(JSON.stringify(event));
  let auxEvent = JSON.parse(JSON.stringify(ideaEvent));
  for (let key in event) {
    if (key in auxEvent) {
      if (isComplexObject(key)) {
        for (let i = 0; i < event[key].length; i++) {
          //copy another instance of a blank complex object into the array
          auxEvent[key].push(JSON.parse(JSON.stringify(ideaEvent[key][0])));
          for (let keyFromComplex in event[key][i]) {
            auxEvent[key][i][keyFromComplex] = event[key][i][keyFromComplex];
          }
        }
        //remove last blank element from the complex object array
        auxEvent[key].pop();
      }else {
        auxEvent[key] = event[key];
      }
    }
  }
  return auxEvent;
}
function isComplexObject (key) {
  let ret = false;
  for (let i = 0; i < COMPLEX_KEYS.length; i++) {
    if (COMPLEX_KEYS[i] == key) {
      ret = true;
    }
  }
  return ret;
}

function extractDasObject (object, event,index) {
  
  let IP4ToDas = ((object.IP4[index] != undefined) ? object.IP4[index] : "");
  let portToDas = ((object.Port != undefined) ? object.Port : "");

  //for target or node in IDEA format
  return {
    event: {
      metaData: {
        DetectTime: "" + event.DetectTime
      },
      correlationData: {
        _id: "" + event.ID
      },
      payloadData: {
        IP4: IP4ToDas,
        Proto: "" + concat(object.Proto),
        Port: portToDas,
        Hostname: "" + concat(object.Hostname)
      }
    }
  };
}

function extractDasCategory (category, event) {
  return {
    event: {
        metaData: {
            DetectTime: "" + event.DetectTime
        },
        correlationData: {
            _id: "" + event.ID
        },
        payloadData: {
            category: "" + category
        }
    }
  };
}


function extractDasQuality (quality, event) {
  if (event.EntityReputation==null || event.EntityReputation === "") {
    event.EntityReputation = 99.0;
  }
  if (quality.IpRecurrence==null || quality.IpRecurrence === "") {
    quality.IpRecurrence = 99.0;
  }
  if (quality.Score==null || quality.Score ==="") {
    quality.Score = 99.0;
  }
  if (quality.Inputs.Completeness==null || quality.Inputs.Completeness==="") {
    quality.Inputs.Completeness = 99.0;
  }
  if (quality.Inputs.SourceRelevance==null || quality.Inputs.SourceRelevance==="") {
    quality.Inputs.SourceRelevance = 99.0;
  }
  if (quality.Inputs.AlertFreshness==null || quality.Inputs.AlertFreshness==="") {
    quality.Inputs.AlertFreshness = 99.0;
  }
  return {
    event: {
        metaData: {
            DetectTime: "" + event.DetectTime
        },
        correlationData: {
            _id: "" + event.ID
        },
        payloadData: {
            Quality: "" + quality.Score,
            Reputation: "" + event.EntityReputation,
            Completeness: "" + quality.Inputs.Completeness,
            IpRecurrence: "" + quality.IpRecurrence,
            SourceRelevance: "" + quality.Inputs.SourceRelevance,
            AttackFreshness: "" + quality.Inputs.AlertFreshness
        }
    }
  };
}

function concat(object_element) {
  let result = object_element[0];
  for (let i = 1; i < object_element.length; i++) {
    result = result + "," + object_element[i];
  }
  return result;
}

function buildMetaAlert (wso2Meta) {
  wso2Meta = JSON.parse(JSON.stringify(wso2Meta))
  let auxMeta = {};
  copyKeys(wso2Meta.event.metaData, auxMeta);
  copyKeys(wso2Meta.event.payloadData, auxMeta);
  auxMeta.Source = {};
  auxMeta.Target = {};
  copyComplexKeys(wso2Meta.event.payloadData, auxMeta);
  return auxMeta;
}
function copyKeys (inObject, outObject) {
  for (let key in inObject) {
    if (!key.includes('Source_') && !key.includes('Target_')) {
      outObject[key] = inObject[key];
    }else if (key == '_id') {
      outObject.ID = inObject._id;
    }
  }
}
function copyComplexKeys (inObject, outObject) {
  for (let key in inObject) {
    if (key.includes('Source_')) {
      let newKey = key.replace("Source_", "");
      if ('Source' in outObject) {
        outObject.Source[newKey] = inObject[key]
      }
    }else if (key.includes ('Target_')) {
      let newKey = key.replace("Target_", "");
      if ('Target' in outObject) {
        outObject.Target[newKey] = inObject[key]
      }
    }
  }
}

module.exports.buildIdeaEvent = buildIdeaEvent;
module.exports.extractDasObject = extractDasObject;
module.exports.extractDasCategory = extractDasCategory;
module.exports.buildMetaAlert = buildMetaAlert;
module.exports.extractDasQuality = extractDasQuality;
